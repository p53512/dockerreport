# DockerReport



## 基礎命令


`-i` :`-interactive` 啟動互動模式，保持標準輸入的開放。

`-t` : `-tty`讓 Docker 分配一個虛擬終端機(pseudo-TTY)，並且綁定到容器的標準輸出上。

`-d` : `-detach` 是讓容器在背景執行。

`--publish` : 配發實體的網路連接埠給容器使用。

`--force` : 強制刪除。

`--name` : 幫容器命名。

`--env` : 幫容器命名。

`--rm` 選項可以讓它結束後自動移除，但得先用 docker stop 停止 container 才會觸發移除，那倒不如使用 `docker rm -f` 指令還比較直接一點。

`-v $PWD:/source` 把本機目錄綁定到 container，$PWD 為執行指令時的當下目錄，/source 則是 container 裡的絕對路徑

`-w /source` 是進去 container 時，預設會在哪個路徑下執行指令

`node:14-alpine` image 名稱，這裡用了 Alpine 輕薄短小版

`npm -v` 在 container 執行的指令，可以視需求換成其他指令

Run 開啟應用程式。

Logs 查看日誌檔內容

Top 查看容器中的程序

Inspect  取得容器的詳細資訊

rm 配合 container id 刪除容器 不會詢問要不要刪除

rmi 配合 image name 刪除映像檔

```
指令檢查看看版本
docker --version
```
```
 環境中目前有哪些 image 
docker images
```
```
 停止container 
stop container
```
```
 目前有哪些 container 在運行中
    1. 檢視所有container 包含已停止
    docker ps -a
    2. docker ps
```
```
 重新啟動 container
docker start CONTAINER_ID
```
```
 顯示容器正在使用多少資源
docker container stats
```

## Demo


```
Docker container run diamol/ch02-hello-diamol
```

**Output**

```
---------------------
Hello from Chapter 2!
---------------------
My name is:
f71fffa08f9e   -> 本機名稱
---------------------
Im running on:
Linux 5.10.104-linuxkit aarch64 ->作業系統
---------------------
My address is:
inet addr:172.17.0.2 ->IP地址 Bcast:172.17.255.255 Mask:255.255.0.0
---------------------
```

目前有哪些 container 在運行中 + -a 可以看到已經離線曾經運行過的container
docker container ps -a

再輸入一次看跟之前的差異
Docker container run diamol/ch02-hello-diamol

再觀察一次之前差異
docker container ps -a

docker rm id
docker rmi diamol/ch02-hello-diamol

在剛剛Demo基本可以看到Docker主要步驟為 建置(build) 、 共享(share) 、 執行(run)。

## 什麼是容器
docker的容器，可以想像成是箱子，
會有自己的本機名稱、IP地址、docker的檔案系統、儲存空間，這些都是由docker所管理，構成我們所謂的箱子。

用來協助處理**軟體密集度**(density)、**隔離環境**(isolation)

## Demo
```
docker container run --interactive --tty diamol/base
```
hostname

date

docker container top 

docker container logs

docker container inspect 

docker container ls -a or --all

不要停止 container 而要退出 docker container 的terminal 需要輸入 ctrl + p之後再輸入 ctrl + q 的按鍵

or

exit
```
docker container run --detach --publish 8088:80 diamol/ch02-hello-diamol-web
```

http://localhost:8088

docker container stats

docker container exec 4c5 ls /usr/local/apache2/htdocs 

docker container cp docker.html 4c5:/usr/local/apache2/htdocs/index.html

ctrl + c 退出

## Docker 名詞講解

```
 Docker engine (管理核心):
  管理映像檔，需要時下載映像檔
  會持續執行在作業系統的後台運作
```

```
 Docker API:
  HTTP的標準REST API，engine通過API提供所有功能
  可從engine設定從本機存取API(預設設置)，或設定為網路上其他電腦可以存取
```

```
 Docker終端對話視窗介面(Command-line interface, CLI):
   API 的客戶端，將命令送至API讓engine完成工作
```

